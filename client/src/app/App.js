import React, {Component} from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import DescriptionForm from '../modules/description/Description';
import ClanForm from '../modules/clan/Clan';
import Home from '../modules/Home';
import {Menu, List} from 'semantic-ui-react';
import AttributeDistribution from '../modules/attributeDistribution/AttributeDistribution';
import attributeForm from '../modules/attribute/Attribute'


class App extends Component {
    state = {};

    handleItemClick = (e, {name}) => this.setState({activeItem: name});

    render() {
        const {activeItem} = this.state;
        return (
            <BrowserRouter>
                <div>
                    <header>
                        <Menu pointing stackable secondary>
                            <Menu.Item name='VampGen' as={Link} to='/' active={activeItem === 'VampGen'}
                                       onClick={this.handleItemClick}>
                                VampGen
                            </Menu.Item>
                            <Menu.Item name='description' as={Link} to='/description'
                                       active={activeItem === 'description'} onClick={this.handleItemClick}>
                                Créer Vampire
                            </Menu.Item>
                        </Menu>
                    </header>
                    <Route exact path="/" component={Home}/>
                    <Route path="/description" component={DescriptionForm}/>
                    <Route path="/clan" component={ClanForm} {...this.props}/>
                    <Route path="/attribute" component={attributeForm}/>
                    <Route path="/attributeDistribution" component={AttributeDistribution} {...this.props}/>
                    <footer>
                        <List>
                            <List.Header>
                                <h2>Contact</h2>
                            </List.Header>
                            <List.Item size="large">
                                <List.Icon name='facebook official' />
                                <List.Content>Facebook</List.Content>
                            </List.Item>
                            <List.Item size="large">
                                <List.Icon name='twitter square' />
                                <List.Content>Twitter</List.Content>
                            </List.Item>
                            <List.Item size="large">
                                <List.Icon name='mail' />
                                <List.Content size="large">
                                    <a href='mailto:jack@semantic-ui.com'>vampGen@gmail.com</a>
                                </List.Content>
                            </List.Item>
                        </List>
                    </footer>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
