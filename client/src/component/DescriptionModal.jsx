import React, {Component} from 'react';
import {Button, Header, Icon, Modal} from 'semantic-ui-react';

class DescriptionModal extends Component {
    state = {modalOpen: false};

    handleOpen = () => this.setState({modalOpen: true});

    handleClose = () => this.setState({modalOpen: false});

    render() {
        return (
            <Modal trigger={<Icon circular name="question" onClick={this.handleOpen}/>}
                   open={this.state.modalOpen}
                   onClose={this.handleClose}
            >
                <Header content={this.props.title}/>
                <Modal.Content content={this.props.body}/>
                <Modal.Actions>
                    <Button color="green" onClick={this.handleClose}>
                        <Icon name="checkmark"/> Ok
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default DescriptionModal;
