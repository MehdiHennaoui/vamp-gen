import React, {Component} from 'react';
import {Message} from 'semantic-ui-react';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';



const rangeField = ({input, meta: { touched, error, warning }}) => (

        <div className="input_range">
            {touched &&
            ((error && <span className="ui negative message error">{error}</span>) ||
            (warning && <span className="ui negative message error">{warning}</span>))}
            <InputRange {...input}  minValue={1} maxValue={5} value={input.value} onChange={input.onChange}/>
        </div>

);

export default rangeField;
