import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';
import promiseMiddleware from 'redux-promise-middleware';
import { loadState, saveState } from './utils/localStorage';
import App from './app/App';
import activeImageReducer from './modules/clan/redux/reducerActiveImg';
import clanReducer from './modules/clan/redux/reducerClans';
import attributeReducer from './modules/attributeDistribution/redux/reducer';
import 'semantic-ui-css/semantic.min.css';
import './app/App.css';

const rootReducer = combineReducers({
    form: formReducer,
    clans: clanReducer,
    activeImage: activeImageReducer,
    attribute: attributeReducer
});

const persistedState = loadState();

const store = createStore(
    rootReducer,
    persistedState,
    compose(
        applyMiddleware(
           promiseMiddleware()
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    ),

);

store.subscribe(() => {
    saveState({
        form: store.getState().form,
        clan: store.getState().clan,
        activeImage: store.getState().activeImage
    });
});

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'),
);
