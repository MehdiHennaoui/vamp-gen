import React from 'react';
import { Button, Container, Grid, Icon } from 'semantic-ui-react';
import {Link}  from 'react-router-dom';

const Home = () => {
    return (
        <Container fluid className="home">
            <div className="home_top">
                <Container text>
                <p>Vamp Gen est un outil pour aidé les joueurs à créer leurs personnages pour le jeu de rôle papier vampire la mascarade</p>
                <div>
                    <Button color='red'>
                        <Link to="/description">Créer un personnage</Link>
                    </Button>
                </div>
                </Container>
            </div>
            <div className="home_content">
                <Container>
                    <Grid divided="computer vertically" stackable centered columns='equal'>
                        <Grid.Column>
                            <Icon size="huge" name="male"/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eos illum quae temporibus. Assumenda consequatur, debitis dolores eius, hic labore laborum molestias necessitatibus nisi quasi quidem repellendus saepe tempora vero.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ex fuga ipsa laborum quia repudiandae saepe ut veniam voluptatem voluptates? Animi, architecto aut eligendi possimus quas quibusdam tenetur! Sapiente, sint.</p>
                        </Grid.Column>
                        <Grid.Column>
                            <Icon size="huge" name="save"/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eos illum quae temporibus. Assumenda consequatur, debitis dolores eius, hic labore laborum molestias necessitatibus nisi quasi quidem repellendus saepe tempora vero.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi, reprehenderit temporibus! A accusamus dicta enim hic in inventore laudantium maxime modi molestias, nobis perspiciatis quo repudiandae sint ut voluptas voluptates?</p>
                        </Grid.Column>
                        <Grid.Column>
                            <Icon size="huge" name="users"/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eos illum quae temporibus. Assumenda consequatur, debitis dolores eius, hic labore laborum molestias necessitatibus nisi quasi quidem repellendus saepe tempora vero.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, ex facere hic necessitatibus nobis pariatur repellendus sequi. Ab aperiam architecto, eaque, exercitationem expedita hic obcaecati officiis provident quibusdam totam, ut?</p>
                        </Grid.Column>

                    </Grid>
                </Container>
            </div>
        </Container>
    );
};

export default Home;