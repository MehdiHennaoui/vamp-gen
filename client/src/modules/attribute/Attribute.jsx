import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Container } from "semantic-ui-react"
import AttributeForm from './AttributeForm'

class Attribute extends Component {
    submitAttribute (values) {
        console.log("attributes: ", values);
        this.props.history.push('/attributeDistribution');
    }
    render() {
        return (
            <Container className="attribute">
                <h1>Formulaire d'attributs</h1>
                <p>Nous voici maintenant à une étape qui fut l'origine de nombreuses heures de reflexion fastidieuses chez toute une génération de joueurs...Non ? Vous ne voyez pas ? Mais si, ça y est ! Oui, c'est ça, je veux bien sûr parler de l'étape de répartition des points de caractéristiques de votre personnage. Ceci permet d'indiquer en quelque sorte les points forts et les points faibles du personnage, du type : "mon personnage est agile, mais il n'est pas très attentif" ou encore "mon personnage est fort comme un ours (...) mais il est nul en relations sociales", etc...</p>
                <p>Dans le jeu Vampire : La Mascarade, vous définissez votre personnage en 3 critères qui sont : le Physique (qui regroupe votre force, votre agilité et votre endurance), le Social (qui regroupe votre charisme, votre apparence et votre aptitude à manipuler les gens qui vous entoure) et le Mental (qui se compose de votre capacité à percevoir les choses, votre intelligence en terme de QI et votre astuce en général).</p>
                <p>Il vous faut donc à présent déterminer un ordre d'importance que vous accordez à ces critères selon le profil que vous souhaitez donner à votre personnage. Pour simplifier, vous pouvez par exemple choisir de créer un personnage faible en Physique, fort en Social et moyen en Mental.</p>
                <p>Quoiqu'il en soit, c'est votre personnage : sélectionnez donc dans les listes ci-dessous le profil qui vous convient le mieux.</p>
                <AttributeForm onSubmit={this.submitAttribute.bind(this)} {...this.props}/>
            </Container>
        );
    }
}

export default withRouter(Attribute);
