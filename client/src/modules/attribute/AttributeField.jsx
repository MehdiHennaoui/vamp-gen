import React, { Component } from 'react';
import {Select, Message, Form} from 'semantic-ui-react';

const attributesOptions = [
    {text: "Fort", value: 10},
    {text: "Moyen", value: 8},
    {text: "Faible", value: 6}
];

const AttributeField = (props) => {
    const {attributes:{ translate }, meta: {asyncValidating, touched, error}, input} = props;
    return (
        <Form.Group inline>
        <Form.Select
            {... input}
            label={translate}
            value={input.value}
            options={attributesOptions}
            onChange={(param, data) => {
                input.onChange(data.value)
            }}/>
        {touched && error && <Message negative>{ error }</Message>}
        </Form.Group>
        )
};

export default AttributeField;
