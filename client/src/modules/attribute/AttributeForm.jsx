import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import newId from '../../utils/newid';
import {Button, Form, Message}  from 'semantic-ui-react';
import AttributeField from './AttributeField';
import asyncValidate from './asyncValidate';
import validate from './validate';

const attributes = [
    {name: "physical", translate: "Physique : "},
    {name: "social", translate: "Social : "},
    {name: "mental", translate: "Mental : "},
];


class AttributeFormContent extends Component {
    render() {

        return (
            <Form.Group className="attribute_form">
                {
                    attributes.map((attribute, index) => (
                        <Field key={newId()} name={attribute.name} attributes={attribute} component={AttributeField}/>
                    ))
                }
            </Form.Group>
        );
    };
}

const AttributeForm = (props) => {
    const { valid } = props;
    return (
        <Form onSubmit={props.handleSubmit}>
            <AttributeFormContent/>
            <Form.Field>
            {valid ?<Button type="submit" content="valider" positive/> :
                [ <Message negative content="Il faut pour chaque critère un niveau différent"/>,<Button type="submit" content="valider" disabled negative/>]}
            </Form.Field>
                </Form>
    )
}

export default reduxForm({
    form: 'attribute',
    validate,
    asyncValidate,
    // asyncBlurFields: ['social','physical', 'mental'],
    destroyOnUnmount: false
})(AttributeForm)

