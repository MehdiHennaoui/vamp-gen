const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const asyncValidate = ( values ) => {
    return sleep(1000).then(()=> {
        if(values.physical === values.social || values.physical === values.mental || values.social === values.mental) {
           throw { _error: 'Il faut pour chaque critère un niveau différent' }
        }
    })
};

export default asyncValidate;
