const validates = values => {
    const erros = {};
    const messageErros = 'Ce Champ est requis';
    if (!values.physical) {
        erros.physical = messageErros;
    }
    if (!values.social) {
        erros.social = messageErros;
    }
    if (!values.mental) {
        erros.mental = messageErros;
    }
    return erros;
};

export default validates;
