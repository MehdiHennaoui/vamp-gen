import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Container } from 'semantic-ui-react'
import AttributeDistributionForm from'./AttributeDistributionForm';

class AttributeDistribution extends Component {
    submitAttribute (values) {
        console.log("attributesDistribution: ", values);

    }
    render() {
      return (
          <Container className="attribute_distribution">
              <h1>Distribution des points d'attributs</h1>
              <p>Maintenant que vous avez déterminé un ordre d'importance pour vos caractéristiques, vous pouvez répartir un certain nombre de points entre elles. Chaque domaine de caractéristiques (Physique, Social, Mental) est divisé en 3 Attributs. </p>
              <p>Selon votre niveau dans un domaine (Fort, Moyen, Faible), vous possédez un nombre plus ou moins grand de points à répartir dans les Attributs de ces domaines. </p>
              <p>Vous possédez à la création de votre personnage d'un point gratuit dans chacun de vos Attributs. Le nombre de points que vous avez dans un Attribut est représenté par le nombre de cases cochées à côté du nom de l'Attribut en question.</p>
              <AttributeDistributionForm onSubmit={this.submitAttribute} />
          </Container>
      );
    }
}

export default withRouter(AttributeDistribution);
