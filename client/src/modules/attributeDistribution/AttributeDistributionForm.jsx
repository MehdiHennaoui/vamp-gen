import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators,compose} from 'redux';
import {reduxForm, formValueSelector, Field, change} from 'redux-form';
import DescriptionModal from '../../component/DescriptionModal';
import newId from '../../utils/newid';
import rangeField  from '../../component/rangeField';
import asyncValidate from './asyncValidate'
import {set_sum_physical, set_sum_mental, set_sum_social} from './redux/actions';
import {Form, Label, Button, Message, Grid} from 'semantic-ui-react';

const AttributeDistributionData = [
    {
        nameAttribute: "physical", translate: "Physique",
        radios: [
            {
                label: "Force: ",
                description: "Il s'agit de la force physique de votre personnage.",
                nameValue: "strength"
            },
            {
                label: "Dextérité: ",
                description: "Il s'agit de l'habileté, de la souplesse de votre personnage.",
                nameValue: "dexterity"
            },
            {
                label: "Vigueur: ",
                description: "Il s'agit de la résistance physique de votre personnage.",
                nameValue: "stamina"
            }]
    },
    {
        nameAttribute: "social", translate: "Social",
        radios: [
            {
                label: "Charisme: ",
                description: "Il s'agit de l'attirance, du magnétisme qu'exerce inconsciemment votre personnage sur les autres personnes.",
                nameValue: "charisma"
            },
            {
                label: "Manipulation: ",
                description: "Il s'agit de la capacité que possède votre personnage à utiliser les autres personnes afin de servir vos buts.",
                nameValue: "manipulation"
            },
            {
                label: "Apparence: ",
                description: "Il s'agit de l'apparence physique de votre personnage.",
                nameValue: "appearance"
            }]
    },
    {
        nameAttribute: "mental", translate: "Mental",
        radios: [
            {
                label: "Perception: ",
                description: "Il s'agit de l'attirance, du magnétisme qu'exerce inconsciemment votre personnage sur les autres personnes.",
                nameValue: "perception"
            },
            {
                label: "Intelligence: ",
                description: "Il s'agit du quotient intellectuel de votre personnage..",
                nameValue: "intelligence"
            },
            {
                label: "Astuce: ",
                description: "Il s'agit de la capacité de bon sens que possède votre personnage.",
                nameValue: "wits"
            }]
    },

];

const caculSumOfSkill = (element1, element2, element3, element4) => {
    const sumSkill = (element2 + element3 + element4);
    const sum = element1 - sumSkill;
    change('attributeDistribution', element1, sum);
    return sum;
};

class AttributeDistributionFormContent extends Component {

    state= {

    }


    findError = (sum, name) => {
        if(sum < 0) {
            return <Message negative>Vous n'avez pas assez de points pour le {name}</Message>
        }
        if(sum > 0) {
            return <Message negative>Vous devez dépensé tous vos points en {name}</Message>
        }
    };

    findActionsSum = (name, sum) => {
        if(name === "physical") {
            return this.props.actions.set_sum_physical(sum);
        }
        if(name === "social") {
            return this.props.actions.set_sum_social(sum);
        }
        else {
            return this.props.actions.set_sum_mental(sum);
        }
    };


    sumAttributeDistribution = (sumPhysical, sumSocial, sumMental) => {
      const sum =   sumPhysical + sumSocial + sumMental;
      return sum;

    };

    findErrorSumForm = (sum) => {
        if(sum < 0) {
            return [<Message negative key={newId()}>Vous n'avez pas dépensez tous vos points</Message>,<Button key={newId()}  type="submit" disabled negative content="valider" />]
        }
        if(sum > 0) {
            return [<Message negative key={newId()}>Vous avez dépensé trop points</Message>,<Button key={newId()}  type="submit" disabled negative content="valider" />]
        }else{
            return <Button key={newId()} positive  type="submit" content="valider"/>
        }
    };

    render() {

        const { sumSocial, sumPhysical, sumMental } = this.props;

        return (
            <div>
            <Form.Group key={newId()} widths='equal'>
                {AttributeDistributionData.map((attribute, index) => {
                    const sumSkill = caculSumOfSkill(this.props[attribute.nameAttribute], this.props[attribute.radios[0].nameValue], this.props[attribute.radios[1].nameValue] , this.props[attribute.radios[2].nameValue]);
                    {this.findActionsSum(attribute.nameAttribute,sumSkill)}
                    return (<Form.Field key={newId()}>
                        <h2>{attribute.translate}</h2>
                        <h2>{sumSkill}</h2>
                        {this.findError(sumSkill, attribute.nameAttribute)}
                        {attribute.radios.map((radio) => {
                            return (
                                <Form.Field vertical key={newId()}>
                                    <div className="">
                                    <Label horizontal>{radio.label}</Label>
                                    <DescriptionModal title={radio.label} body={radio.description}/>
                                    <Field
                                        name={radio.nameValue}
                                        component={rangeField}
                                    />
                                    </div>
                                </Form.Field>
                            )
                        })
                        }
                    </Form.Field>)
                })}
            </Form.Group>
            <div>
                {this.findErrorSumForm(this.sumAttributeDistribution(sumPhysical, sumSocial, sumMental))}
            </div>
            </div>
        );
    }
}

let AttributeDistributionForm = (props) => {

    return (
        <Form onSubmit={props.handleSubmit}>
            <AttributeDistributionFormContent {...props}/>
        </Form>
    )
};

const selectorAttributePrimary = formValueSelector('attribute');
const selectorAttributeSecondary = formValueSelector('attributeDistribution');

const mapStateToProps =
    state => ({
        strength: selectorAttributeSecondary(state, 'strength'),
        dexterity: selectorAttributeSecondary(state, 'dexterity'),
        stamina: selectorAttributeSecondary(state, 'stamina'),
        charisma: selectorAttributeSecondary(state, 'charisma'),
        manipulation: selectorAttributeSecondary(state, 'manipulation'),
        appearance: selectorAttributeSecondary(state, 'appearance'),
        perception: selectorAttributeSecondary(state, 'perception'),
        intelligence: selectorAttributeSecondary(state, 'intelligence'),
        wits: selectorAttributeSecondary(state, 'wits'),
        physical: selectorAttributePrimary(state, 'physical'),
        mental: selectorAttributePrimary(state, 'mental'),
        social: selectorAttributePrimary(state, 'social'),
        sumSocial: state.attribute.social,
        sumMental: state.attribute.mental,
        sumPhysical: state.attribute.physical
    });

const mapDispatchToProps = dispatch => ( {
    actions: bindActionCreators({set_sum_physical, set_sum_mental, set_sum_social}, dispatch)
});

export default AttributeDistributionForm = compose(
    reduxForm({
        initialValues:{
            strength: 1,
            dexterity: 1,
            stamina: 1,
            charisma: 1,
            manipulation: 1,
            appearance: 1,
            perception: 1,
            intelligence: 1,
            wits: 1,
        },
        destroyOnUnmount: false,
        form: 'attributeDistribution',
        asyncValidate,
    }),
    connect(mapStateToProps, mapDispatchToProps)
)(AttributeDistributionForm);
