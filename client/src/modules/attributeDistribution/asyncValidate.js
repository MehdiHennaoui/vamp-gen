const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const asyncValidate = ( values ) => {
    return sleep(1000).then(()=> {
        console.log('values', values);
        if(values.physical < 0 ) {
           throw { _error: 'Vous n\'avez pas assez de points de physique'}
        }else if(values.social < 0) {
            throw { _error: 'Vous n\'avez pas assez de points social' }
        }else if(values.mental < 0){
            throw { _error: 'Vous n\'avez pas assez de points social' }
        }else if(values.physical !== 0) {
            throw { _error: 'Vous devez dépensé tous vos points en physique' }
        }else if(values.social !== 0) {
            throw { _error: 'Vous devez dépensé tous vos points en social' }
        }else if(values.mental !== 0){
            throw { _error:'Vous devez dépensé tous vos points en mental' }
        }
    })
};

export default asyncValidate;
