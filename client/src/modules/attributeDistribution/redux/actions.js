import {SET_SUM_SKILL_PHYSICAL, SET_SUM_SKILL_MENTAL, SET_SUM_SKILL_SOCIAL} from "./types";


export const set_sum_physical = payload => ({
        type:SET_SUM_SKILL_PHYSICAL,
        payload
    });

export const set_sum_mental = payload => ({
    type:SET_SUM_SKILL_MENTAL,
    payload
});

export const set_sum_social = payload => ({
    type:SET_SUM_SKILL_SOCIAL,
    payload
});