import { SET_SUM_SKILL_PHYSICAL, SET_SUM_SKILL_MENTAL, SET_SUM_SKILL_SOCIAL } from './types';

const initialState = {
    physical: 0,
    mental: 0,
    social: 0
};

export default function attributeReducer (state= initialState, action) {
    switch (action.type) {
        case SET_SUM_SKILL_PHYSICAL:
            return Object.assign({}, state, {
                ...state,
                physical: action.payload,
            });
        case SET_SUM_SKILL_MENTAL:
            return Object.assign({}, state, {
                ...state,
                mental: action.payload,
            });
        case SET_SUM_SKILL_SOCIAL:
            return Object.assign({}, state, {
                ...state,
                social: action.payload,
            });
        default:
            return state;
    }
}