import React, { Component } from 'react';
import ClanForm from './ClanForm';
import { withRouter } from 'react-router';

class Clan extends Component {
    submitClan (values) {
        console.log("clan: ", values);
        this.props.history.push('/attribute');
    }
    render() {
        return (
            <div>
                <h1>Formulaire de Clan</h1>
                <ClanForm onSubmit={this.submitClan.bind(this)}/>
            </div>
        );
    }
}

export default withRouter(Clan);