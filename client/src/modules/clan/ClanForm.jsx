import React, {Component} from 'react';
import {bindActionCreators, compose} from 'redux';
import {Field, reduxForm} from 'redux-form';
import {setActiveImage, getClans} from './redux/actions';
import {connect} from 'react-redux';
import { Button, Message, Container, Dimmer, Loader  } from 'semantic-ui-react'
import './clan-form.css';

class ClanFormContent extends Component {
    componentDidMount = () => {
        this.props.actions.getClans()
    };

    render() {
        const {clans, actions, activeImage} = this.props;
        const setActiveImageWithIndex = (id) => actions.setActiveImage(id);
        if(!clans.clans.length)
        {
            return (
                <Dimmer active>
                    <Loader>Chargement ...</Loader>
                </Dimmer>
            )
        }else {
            return (
                <Container className="clan">
                    { clans.clans.map((clan, index) => (
                        <div className="form-control" key={clan.id + clan.name}>
                            <h2 htmlFor="clan">{clan.translation}</h2>
                            <label>
                                <Field
                                    type="radio"
                                    value={clan.name}
                                    name="clan"
                                    component="input"
                                    className="input-radio"
                                />
                                <img
                                    src={process.env.PUBLIC_URL + '/img/clan/' + clan.image}
                                    alt=""
                                    onClick={() => setActiveImageWithIndex(index)}
                                    //si l'index de l'image correspond à l'index enregistré le store ajouté la class active sinon ajoute rien
                                    className={`image-clan ${ index == activeImage.activeImage  ? 'active' : ''}`}
                                />
                            </label>
                            { index == activeImage.activeImage ? <p>{clan.description}</p> : ''}
                        </div>
                    ))}
                    <div>
                        {activeImage === null ? [<Button type="submit" negative disabled content="valider"/>,<Message negative>Vous devez choisir un clan</Message>] : <Button type="submit" positive content="valider"/>}
                    </div>
                </Container>);

        }
    }
};

const ClanForm = (props) => (
    <form onSubmit={props.handleSubmit}>
        <ClanFormContent {...props}/>
    </form>
);


const mapStateToProps = state => ({
    activeImage: state.activeImage,
    clans: state.clans
});

const mapDispatchToProps = dispatch => ( {
    actions: bindActionCreators({setActiveImage, getClans}, dispatch)
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'clan',
        destroyOnUnmount: false,
    }))(ClanForm);

