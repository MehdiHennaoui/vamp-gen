import {SET_IMAGE, ADD_CLANS} from "./types";
import axios from 'axios';

export const setActiveImage = payload => ({
        type:SET_IMAGE,
        payload
    });

export const getClans = payload => ({
   type: ADD_CLANS,
    payload: axios({
        method:'get',
        url: 'http://127.0.0.1:8000/clans',
        headers: {"Accept": "application/json"},
    })
});