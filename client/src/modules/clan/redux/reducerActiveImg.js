import { SET_IMAGE } from './types';

const initialState = {
    activeImage: null,
};

export default function activeImageReducer (state= initialState, action) {
    switch (action.type) {
        case SET_IMAGE:
            return Object.assign({}, state, {
                activeImage: action.payload,
            });
        default:
            return state;
    }
}