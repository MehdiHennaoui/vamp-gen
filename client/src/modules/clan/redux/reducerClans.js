import { ADD_CLANS }   from './types';

const initialState = {
    clans: []
};


export default function clanReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_CLANS + '_FULFILLED':
            return Object.assign({}, state, {
                clans: action.payload.data,
            });
        default:
            return state;
    }
}



