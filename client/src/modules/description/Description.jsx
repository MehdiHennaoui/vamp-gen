import React, { Component } from 'react';
import DescriptionForm from './DescriptionForm';
import {withRouter} from 'react-router';

class Description extends Component {
    submitDescription = (values) => {
        console.log("description: ", values);
        this.props.history.push('/clan');
    };
    render() {
        return (
            <div>
                <h1>Formulaire de Description</h1>
                <DescriptionForm {... this.props} onSubmit={this.submitDescription}/>
            </div>
        );
    }
}

export default withRouter(Description);