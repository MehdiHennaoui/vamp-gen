import React from 'react';
import {Input, Message, Label, Form} from 'semantic-ui-react';

const DescriptionField = ({
                              input,
                              label,
                              type,
                              meta: {touched, error, warning},
                          }) => (
    <span>

        <Input label={label}>
        <Label>{label}</Label>
            <input  {...input} placeholder={label} type={type} />
        </Input>
        {touched &&
        ((error && <Message error>{error}</Message>) ||
        (warning && <Message error>{warning}</Message>))}
                    </span>
);
export default DescriptionField;
