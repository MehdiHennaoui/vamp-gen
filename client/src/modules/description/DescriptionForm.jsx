import React from 'react';
import {Field, reduxForm, Form} from 'redux-form';
import {
    required,
    alphaNumeric,
    negativeNumber,
    maxLength50,
    minLength2,
} from '../../utils/validate';
import DescriptionModal from '../../component/DescriptionModal';
import DescriptionField from './DescriptionField';
import {Grid, Container, Button} from 'semantic-ui-react'


const DescriptionContent = props => {
    const {valid} = props;

    return (
        <Container className="description">
            <Grid.Row>
                <Field
                    name='player'
                    component={DescriptionField}
                    type="text"
                    validate={[required, maxLength50, minLength2]}
                    label='Nom du joueur* :'
                />
                <DescriptionModal
                    title='Nom du joueur'
                    body='Votre nom réel ou votre pseudo'
                    validate={[required, maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='vampire'
                    component={DescriptionField}
                    type="text"
                    label='Nom du vampire* :'
                    validate={[required, maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
                <DescriptionModal
                    title='Nom du vampire'
                    body='Le nom de votre vampire'
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='age'
                    component={DescriptionField}
                    type="number"
                    label='Age apparent* :'
                    validate={[required, negativeNumber]}
                />
                <DescriptionModal
                    title='age'
                    body="Il s'agit de l'âge (apparent) de votre personnage."
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='nature'
                    component={DescriptionField}
                    type="text"
                    label='Nature* :'
                    validate={[required, maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
                <DescriptionModal
                    title='Nature'
                    body="Il s'agit de votre MOI profond. Ce que vous êtes réellement, et pas forcément ce que vous affichez extérieurement (ça, c'est l'attitude).Exemple : Individualiste."
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='demeanor'
                    component={DescriptionField}
                    type="text"
                    label='Attitude* :'
                    validate={[required, maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
                <DescriptionModal
                    title='attitude'
                    body="Il s'agit de votre comportement, du moins celui que vous affichez. C'est en fait l'image de vous-même que vous désirez renvoyer aux autres. Exemple : Rebelle."
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='concept'
                    component={DescriptionField}
                    type="text"
                    label='Concept :'
                    validate={[maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
                <DescriptionModal
                    title='Concept'
                    body="Il s'agit du métier de votre personnage.Exemple : Médecin."
                />
            </Grid.Row>
            <Grid.Row>
                <Field
                    name='haven'
                    component={DescriptionField}
                    type="text"
                    label='Refuge :'
                    validate={[maxLength50, minLength2]}
                    warn={alphaNumeric}
                />
                <DescriptionModal
                    title='Refuge'
                    body='Là où habite votre personnage.'
                />
            </Grid.Row>

            {valid ? <Button type="submit" content="valider" positive/> :
                <Button type="submit" content="valider" disabled negative/>}

        </Container>

    );
};

const DescriptionForm = (props) => (
    <form onSubmit={props.handleSubmit}>
        <DescriptionContent {...props}/>
    </form>
);


export default reduxForm({
    destroyOnUnmount: false,
    form: 'description',
})(DescriptionForm);
