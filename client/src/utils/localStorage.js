export const loadState = () => {
    try {
        const serialiseSate = localStorage.getItem('state');
        if (serialiseSate == null) {
          return undefined;
        }
        return JSON.parse(serialiseSate);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (state) => {
    try {
       const serializedState = JSON.stringify(state);
       localStorage.setItem('state', serializedState);
    } catch (err) {

    }
};