export const translateAttributeIntoPoints = (skill) => {
    if(skill === 'strong') {
        return 7;
    }
    if(skill === 'medium') {
        return 5;
    }
    else {
        return 3;
    }
};