
const maxLength = max => value => value && value.length > max ? 'Le nombre de caractères de doit pas dépasser ' + max + ' mots': undefined
const minLength = min => value => value && value.length <= min ? 'Ce champ doit avoir un mot de ' + min + ' ou plus de caractères': undefined

export const required = value => (value ? undefined: 'ce champ requis')

export const alphaNumeric = value => value && /[^a-zA-Z0-9 ]/i.test(value) ? 'Ce champ ne doit avoir que des numéros et des lettres': undefined
export const negativeNumber = value => value < 0 ? 'Ce champ doit etre supérieur à zero': undefined

export const maxLength50 = maxLength(50);
export const minLength2 = minLength(2);

