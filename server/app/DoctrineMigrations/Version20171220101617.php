<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171220101617 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discipline (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, translation VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discipline_clan (discipline_id INT NOT NULL, clan_id INT NOT NULL, INDEX IDX_4A8A94E2A5522701 (discipline_id), INDEX IDX_4A8A94E2BEAF84C8 (clan_id), PRIMARY KEY(discipline_id, clan_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mental (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clan (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, translation VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_9FF6A30C5E237E06 (name), UNIQUE INDEX UNIQ_9FF6A30CB469456F (translation), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vampire (id INT AUTO_INCREMENT NOT NULL, haven VARCHAR(255) DEFAULT NULL, name LONGTEXT NOT NULL, nature LONGTEXT NOT NULL, concept LONGTEXT NOT NULL, demeanor LONGTEXT DEFAULT NULL, generation INT NOT NULL, date_update DATETIME NOT NULL, date_create DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discipline_clan ADD CONSTRAINT FK_4A8A94E2A5522701 FOREIGN KEY (discipline_id) REFERENCES discipline (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discipline_clan ADD CONSTRAINT FK_4A8A94E2BEAF84C8 FOREIGN KEY (clan_id) REFERENCES clan (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE discipline_clan DROP FOREIGN KEY FK_4A8A94E2A5522701');
        $this->addSql('ALTER TABLE discipline_clan DROP FOREIGN KEY FK_4A8A94E2BEAF84C8');
        $this->addSql('DROP TABLE discipline');
        $this->addSql('DROP TABLE discipline_clan');
        $this->addSql('DROP TABLE mental');
        $this->addSql('DROP TABLE clan');
        $this->addSql('DROP TABLE vampire');
    }
}
