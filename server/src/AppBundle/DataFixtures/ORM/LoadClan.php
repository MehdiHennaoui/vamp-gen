<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Clan;

class LoadClan extends Fixture {
    public function load(ObjectManager $manager)
    {
        $clans = array(
          "Brujah" => array("Ces rebelles sont le soutien le plus puissant des Anarch (jeunes rebelles). Ils ne respectent aucune autorité et ne reconnaissent aucun dirigeant.", "Brujah", "Brujah.gif"),
            "Gangrel" => array("Solitaires et rustiques, on les appelle aussi 'les étrangers'. Ce sont les vampires qui peuvent changer de forme le plus souvent.", "Gangrel", "Gangrel.gif"),
            "Malkavian" => array("Bien que les autres les croient complètement fous, les membres de ce clan ont une vision et une sagesse étranges.", "Malkavien", "Malkavian.gif"),
            "Nosferatu" =>array('Ces créatures hideuses sont mises à l\'écart et incomprises des autres clans.','Nosferatus', 'Nosferatu.gif'),
            'Toreador' => array('Bien qu\'étant connus pour leurs penchants hédonistes, ils préfèrent se voir comme des artistes. Seuls ceux qui sont "valables" sont acceptés.', 'Toreador', 'Toreador.gif'),
            'Tremere' => array('Sophistiqués et d\'un goût trop raffiné, ces vampires conservateurs et calculateurs sont les chefs de la Camarilla.', 'Tremere', 'Tremere.gif'),
            'Ventrue' => array('Magiciens qui viennent d\'un ancien pouvoir, c\'est un clan particulièrement limité et hiérarchisé.','Ventrue', 'Ventrue.gif'),
            'Lasombra'=> array('Les Lasombras sont un Clan de manipulateurs nés. Leurs ennemis de toujours sont les Ventrues avec lesquels ils se disputent la suprématie sur les vampires. Au contraire des Ventrues, les Lasombras préfèrent les manipulations dans l\'ombre au pouvoir réel direct. Ils sont un des deux clans fondateurs du Sabbat dont ils sont les principaux leaders politiques.','Lasombra', 'Lasombra.png'),
            'Tzimisce'=> array('L\'autre Clan fondateur du Sabbat, dont ils sont l\'âme et les guides spirituels. Ils sont originaires de Transylvanie, où ils sont à l\'origine des mythes comme celui de Dracula (qui était un membre de ce Clan). Ils ont le pouvoir de manipuler la matière vivante : que ce soit la leur ou celle des autres.', 'Tzimisce', 'Tzimisce.png'),
            'Assamite' => array('Un clan indépendant, issu du monde islamique, dont le QG se trouve en Turquie. Ce clan prône le respect des humains et la Diablerie sur les autres vampires de Génération inférieure.', 'Assamite', 'Assamite.gif'),
            'Followers of Set' => array('Sont les descendants du dieu égyptien Seth. Ils n\'ont aucun intérêt pour la politique des vampires. Ils ont uniquement deux occupations : la corruption de l\'humanité par la drogue, la luxure, et d\'une manière générale tous les péchés capitaux, et le réveil de Seth, leur fondateur. Ils ne sont pas appréciés des autres vampires qui se méfient de ces puissants manipulateurs apportant le chaos dans l\'humanité','Disciples de Seth', 'Followers.gif'),
            'Giovanni' => array('Autant une famille qu\'un clan, des nécromanciens italiens.','Giovanni', 'Giovanni.jpg'),
            'Ravnos' => array('L\'un des clans les plus méprisé. Ils sont interdits de séjour dans presque toutes les villes, mais souvent tolérés, par crainte de voir tout le clan débarquer et ravager le territoire. Ils ont beaucoup hérité des gitans, ce qui les rend menteurs, voleurs, dissimulateurs, et les classe probablement parmi les moins dignes de confiance de tous, mais certains possèdent la connaissance des Romanichels...', 'Ravnos', 'Ravnos.gif'),
        );
        $id = 1;
        foreach ($clans as $name => list($description, $translation, $image)) {
            $clan = new Clan();
            $clan->setName($name);
            $clan->setDescription($description);
            $clan->setTranslation($translation);
            $clan->setImage($image);
            $manager->persist($clan);
            $this->addReference($name, $clan);
            $id++;
        }

        $manager->flush();
    }
}
