<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Discipline;


class LoadDiscipline extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $disciplines = array(
            "Dementation" => array("L’aliénation procure aux Malkavian le pouvoir de transmettre leur folie, leur sagesse peut être, à une victime , emmenant le sujet dans leur monde déformé. La partie effrayante est qu’elle canalise la folie plus qu’elle ne la crée, révélant par là, la démence latente de chaque individu.", "Aliénation", array("Malkavian")),
            "Animalism" => array("La discipline de l’animalisme offre au caïnite une intense empathie et un grand pouvoir sur le règne animal. Permettant d’appeler, de discuter avec les animaux, son plus grand pouvoir est celui qu’elle offre sur la Bête qu’il y a en chaque mortel et en chaque Vampire.", "Animalisme", array("Gangrel", "Nosferatu", "Ravnos", "Tzimisce")),
            "Auspex" => array("Un vampire doté de la discipline d’augure possède la vision divine, capable de voir plus loin que n’importe quel mortel, de lire l’âme des hommes et même leurs esprits. Les sensations sont tellement puissantes qu’un vampire peut se retrouver déconnecté de la réalité par la force de l’augure. Et certains seigneurs du haut de leur donjon surveillent toute l’étendue de leur domaine grâce à cette discipline.", "Augure", array("Malkavian", "Toreador", "Tremere", "Tzimisce")),
            "Celerity" => array("En période de stress, certains caïnites ayant la célérité peuvent se déplacer à une vitesse incroyable, devenant flous aux yeux des mortels et immortels ne possédant pas cette discipline. La célérité offre la vitesse dans les espaces sauvages, la précision dans l’art et la prouesse dans l’art du combat.", "Célérité", array("Brujah", "Toreador")),
            "Chimerstry" => array("Cette discipline permet aux Ravnos de créer des illusions complètes et des hallucinations qui embrouillent les sens d’autrui... De l’illusion légère qui détourne l’attention à l’éveil de crainte séculaire qui terrifie l’être immortel, la Chimérie est la science de l’illusion sans limite.", "Chimérie", array("Ravnos")),
            "Dominate" => array("La Domination est utilisée pour contrôler les pensées et les actions d’autrui. Manipuler l’âme d’autrui est très mal considéré mais nombreux sont les vampires à utiliser cette discipline sans scrupule pour se faire obéir sans condition.", "Domination", array("Giovanni", "Lasombra", "Malkavian", "Tremere", "Ventrue")),
            "Fortitude" => array("Les Caïnites possédant cette discipline sont capables de supporter d’incroyables douleurs physiques sans flancher. De plus ceux qui possèdent la Force d’âme peuvent réussir à ne pas plier devant le Feu ou le Soleil.", "Force d’âme", array("Gangrel", "Ravnos", "Ventrue")),
            "Necromancy" => array("Cette discipline cherche à explorer les aspects de la mort, à tricher avec, à la causer par un simple toucher. Les pratiquants les plus zélés de Mortis peuvent défier la Mort elle-même.", "Mortis", array("Giovanni")),
            "Obtenebration" => array("L’Obténébration permet à son pratiquant de manipuler une étrange force de ténèbres vivantes, l’essence même de l’ombre faite tangible. Repoussant la lumière, sculptant les ténèbres en arme ou tentacule, la manifestation de l’obténébration panique animaux et mortels.", "Obténébration", array("Lasombra")),
            "Obfuscate" => array("Certains caïnites peuvent se dissimuler à la vision mortelle par l’utilisation de ce redoutable pouvoir qui vise à cacher, tromper, disparaître et se parer d’un masque qui peut dissimuler jusqu’à vos moindres pensées.", "Occultation", array("Lasombra", "Followers of Set", "Malkavian", "Nosferatu")),
            "Presence" => array("C’est la discipline de l’attirance surnaturelle. Avec elle, le Vampire lève des armées, fomente des révoltes et balaie les oppositions des autres Princes. La Présence vous affiche aux yeux du monde comme l’être surnaturel que vous êtes, adulé et vénéré de tous. Vous connaissez le dicton : « Trop beau pour être vrai »…", "Présence", array("Brujah", "Followers of Set", "Toreador", "Ventrue")),
            "Protean" => array("Les Vampires possédant la discipline de Métamorphose peuvent manipuler leur corps pour en révéler la bestialité. Ils font pousser leurs griffes, luire leurs yeux et à une certaine maîtrise, ils se transforment en Loup ou en Corbeau.", "Métamorphose-Protéïsme", array("Gangrel")),
            "Potence" => array("Puissance apporte au Vampire une force bien au dessus des standards caïnites. Avec elle, le non-mort peut projeter un destrier, bondir sur des distances incroyables ou déchirer un adversaire à mains nues.", "Puissance", array("Brujah")),
            "Quietus" => array("Quiétus réunit les deux points d’excellence de l’Assamite : La mort silencieuse et le pouvoir du sang. Elle ouvre des pouvoirs mystiques qui permettent l’excellence dans le meurtre et l’assassinat par la furtivité et la diminution physique d’autrui.", "Quiétus", array("Brujah", "Giovanni", "Lasombra", "Nosferatu")),
            "Serpentis" => array("C’est le leg de Seth à ses enfants. Rappelez vous du Jardin d’Eden ! Serpentis est la marque de la corruption et de la tentation. Par son usage, le Séthite peut briser les plus fermes résolutions ou ébranler les plus fanatiques aussi bien moralement que physiquement.", "Serpentis", array("Followers of Set")),
            "Thaumaturgy" => array("La discipline de Thaumaturgie est l’un des secrets les mieux gardé du clan Tremere. Elle combine la magie païenne hautement ritualisée au pouvoir du Sang des enfants de Caïn. Elle donne accès selon la branche étudiée à de nombreuses spécialisations.", "Thaumaturgie", array("Tremere")),
            "Vicissitude" => array("Là ou la Métamorphose permet d’imiter les dons des créatures de Dieu, la Vicissitude permet de corrompre ces même créatures ou le Vampire lui-même. La femme la plus belle peut être réduite à l’état de créature hideuse, les gardes armés en goules difformes munies de crêtes osseuses ou de griffes gigantesques. C’est la discipline des sculpteurs de chairs qui au contact transforment votre corps en argile.", "Vicissitude", array("Tzimisce"))
        );


        $id = 1;
        foreach ($disciplines as $name => list($description, $translation, $clans)) {
            $discipline = new Discipline();
            $discipline->setName($name);
            $discipline->setDescription($description);
            $discipline->setTranslation($translation);
            foreach ($clans as  $clan) {
                $discipline->addClan($this->getReference($clan));
            }
            $manager->persist($discipline);
            $id++;
        }

        $manager->flush();
    }
}