<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Discipline
 *
 * @ApiResource
 * @ORM\Table(name="discipline")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DisciplineRepository")
 */
class Discipline
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="translation", type="string", length=255, nullable=true)
     */
    private $translation;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Clan", cascade={"persist"})
     */
    private $clans;

    public function __construct()
    {
        $this->clans = new ArrayCollection();
    }

    public function addClan(Clan $clan)
    {
        $this->clans[] = $clan;
    }

    public function removeClan(Clan $clan)
    {
        $this->clans->removeElement($clan);
    }

    public function getClan()
    {
        return $this->clans;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Discipline
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Discipline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set translation
     *
     * @param string $translation
     *
     * @return Discipline
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * Get clans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClans()
    {
        return $this->clans;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Discipline
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
