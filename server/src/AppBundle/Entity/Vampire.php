<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vampire
 *
 * @ApiResource
 * @ORM\Table(name="vampire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VampireRepository")
 */
class Vampire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="haven", type="string", length=255, nullable=true)
     */
    private $haven;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="text")
     */
    private $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="concept", type="text")
     */
    private $concept;

    /**
     * @var string
     *
     * @ORM\Column(name="demeanor", type="text", nullable=true)
     */
    private $demeanor;

    /**
     * @var int
     *
     * @ORM\Column(name="generation", type="integer")
     */
    private $generation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set haven
     *
     * @param string $haven
     *
     * @return Vampire
     */
    public function setHaven($haven)
    {
        $this->haven = $haven;

        return $this;
    }

    /**
     * Get haven
     *
     * @return string
     */
    public function getHaven()
    {
        return $this->haven;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Vampire
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return Vampire
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set concept
     *
     * @param string $concept
     *
     * @return Vampire
     */
    public function setConcept($concept)
    {
        $this->concept = $concept;

        return $this;
    }

    /**
     * Get concept
     *
     * @return string
     */
    public function getConcept()
    {
        return $this->concept;
    }

    /**
     * Set demeanor
     *
     * @param string $demeanor
     *
     * @return Vampire
     */
    public function setDemeanor($demeanor)
    {
        $this->demeanor = $demeanor;

        return $this;
    }

    /**
     * Get demeanor
     *
     * @return string
     */
    public function getDemeanor()
    {
        return $this->demeanor;
    }

    /**
     * Set generation
     *
     * @param integer $generation
     *
     * @return Vampire
     */
    public function setGeneration($generation)
    {
        $this->generation = $generation;

        return $this;
    }

    /**
     * Get generation
     *
     * @return int
     */
    public function getGeneration()
    {
        return $this->generation;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Vampire
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Vampire
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }
}
